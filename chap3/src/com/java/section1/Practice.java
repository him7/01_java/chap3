package com.java.section1;

public class Practice {

	/* -- 메소드명(printStudent)
	 * 이름, 학년(숫자만),반(숫자만),번호(숫자만), 성별(M/F), 성적을 매개변수로받는 메소드를 만들고 
	 * 아래와 같은	 결과를 리턴한다. 
	 * 출력은 실행용클래스 Application에서 실행한다.
	 * 
	 * 단, 성별이 'M'이면 남학생, 'M'이 아니면 여학생으로 리턴처리한다.
	 * 
	 * ex.
	 *		이름 : 홍길동
	 *		학년(숫자만) : 3
	 *		반(숫자만) : 4
	 *		번호(숫자만) : 15
	 *		성별(M/F) : M
	 *		성적 : 85
	 *		
	 *		3학년 4반 15번 홍길동 남학생의 성적은 85이다.
	 * */
	
	public String printStudent(int year, int cs, int num, String name, char gender, int grade) {
		String result = "";
		char upGen = Character.toUpperCase(gender);
		String gen = "";
		
		if(upGen == 'M') {
			gen = "남학생";  
		} else if(upGen == 'F') {
			gen = "여학생"; 
		} else {
			result = "성별 오류입니다";
			return result;
		}
		
		result = year+"학년 "+cs+"반 "+num+"번 "+name+" "+gen+"의 성적은 "+grade+"이다.";
		return result;
	}

}
