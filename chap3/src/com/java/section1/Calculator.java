package com.java.section1;

public class Calculator {

	/*
	 * 두 수를 매개변수로 입력받아 두 수의 합을 구하고
	 * 리턴값을 반환한다.
	 * 
	 * 리턴값 예시
	 * 두 수의 합은 50입니다. 
	 * */
	
	public String sum(int first, int second) {
		String result = "";
		result = "두 수의 합은 "+(first+second)+"입니다.";
		return result;
	}
	
	/*
	 * 두 수를 매개변수로 입력받아 두 수를 뺀값 구하고
	 * 리턴값을 반환한다.
	 * 
	 * 리턴값 예시
	 * 두 수를 뺀 값은 50입니다. 
	 * */ 
	
	public String minus(int first, int second) {
		String result = "";
		result = "두 수를 뺀 값은 "+(first-second)+"입니다.";
		return result;
	}
	
	
}
