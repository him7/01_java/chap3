package com.java.section1;

public class Application {

	
	/**
	 * <pre>
	 *   실행용 메소드입니다.
	 * </pre>
	 * @param args
	 */
	public static void main(String[] args) {
//		두 수의 합은 50입니다. 
		Calculator calc = new Calculator();
		System.out.println(calc.sum(30, 20));
//		두 수를 뺀 값은 50입니다. 
		System.out.println(calc.minus(80, 30));
//		3학년 4반 15번 홍길동 남학생의 성적은 85이다.
		Practice pra = new Practice();
		System.out.println(pra.printStudent(3, 4, 15, "홍길동", 'm', 85));
	}
}
