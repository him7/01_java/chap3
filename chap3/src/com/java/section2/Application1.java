package com.java.section2;

public class Application1 {
	/**
	 * 실행용 메소드
	 * @param args
	 */
	public static void main(String[] args) {
		Practices pra = new Practices();
		System.out.println(pra.practice1());
		System.out.println(pra.practice2());
		System.out.println(pra.practice3());
		pra.practice4();
		pra.practice5();
		pra.practice6();
	}
}
