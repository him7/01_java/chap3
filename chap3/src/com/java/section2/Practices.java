package com.java.section2;

import java.util.Scanner;

public class Practices {

	/* 메소드명 : practice1() */
	/* 키보드로 입력 받은 하나의 정수가 양수이면 "양수다.", 양수가 아니면 "양수가 아니다"를 반환해서
	 * 실행용클래스에서 출력하시오
	 * 
	 * -- 출력 예시 --
	 * 정수 : 9
	 * 양수다.
	 * */
	
	public String practice1() {
		String result = "";
		Scanner sc = new Scanner(System.in);
		System.out.print("숫자 입력 : ");
		int num = sc.nextInt();
		if(num>0) {
			result = "양수다";
		} else if(num<0) {
			result = "음수다";
		} else {
			result = "0이다";
		}
		return result;
	}
	
	/* 메소드명 : practice2() */
	/* 키보드로 입력 받은 하나의 정수가 양수이면 "양수다", 
	 * 양수가 아닌 경우 중에서 0이면 "0이다", 0이 아니면 "음수다"를 반환해서 
	 * 실행용 클래스에서 출력하시오
	 * 
	 * -- 출력 예시
	 * 정수 : 0
	 * 0이다
	 * */
	
	public String practice2() {
		String result = "";
		Scanner sc = new Scanner(System.in);
		System.out.print("숫자 입력 : ");
		int num = sc.nextInt();
		if(num>0) {
			result = "양수다";
		} else if(num<0) {
			result = "음수다";
		} else {
			result = "0이다";
		}
		return result;
	}
	
	/* 메소드명 : practice3() */
	/* 키보드로 입력 받은 하나의 정수가 짝수이면 "짝수다", 짝수가 아니면 "홀수다"를 반환해서
	 * 실해용 클래스에서 출력하시오
	 * 
	 * -- 출력 예시
	 * 정수 : 5
	 * 홀수다
	 * */

	public String practice3() {
		String result = "";
		Scanner sc = new Scanner(System.in);
		System.out.print("숫자 입력 : ");
		int num = sc.nextInt();
		if(num%2==0) {
			result = "짝수다";
		} else if(num%2==1) {
			result = "홀수다";
		} else {
			result = "오류 발생";
		}
		return result;
	}
	
	/* 메소드명 : practice4() */
	/* 모든 사람이 사탕을 골고로 나눠가지려고 한다. 인원수와 사탕 개수를 키보드로 입력 받고 1인당
	 * 동일하게 나눠가진 사탕 개수와 나눠주고 남은 사탕의 개수를 출력하세요.
	 * 
	 * -- 출력 예시
	 * 인원수 : 29
	 * 사탕 개수 : 100
	 * 
	 * 1인당 사탕 개수 : 3
	 * 남는 사탕 개수 : 13
	 * */
	
	public void practice4() {
		Scanner sc = new Scanner(System.in);
		System.out.print("인원수 : ");
		int num1 = sc.nextInt();
		System.out.print("사탕 개수 : ");
		int num2 = sc.nextInt();
		System.out.println("1인당 사탕 개수 : " + (num2/num1));
		System.out.println("남은 사탕 개수 : " + (num2%num1));
	}

	/* 메소드명 : practice5() */
	/* 키보드로 입력 받은 값들을 변수에 기록하고 저장된 변수 값을 화면에 출력하여 확인하세요.
	 * 이 때 성별이 'M'이면 남학생, 'M'이 아니면 여학생으로 출력 처리하세요
	 * 
	 * -- 출력 예시
	 * 이름 : 홍길동
	 * 학년(숫자만) : 3
	 * 반(숫자만) : 4
	 * 번호(숫자만) : 15
	 * 성별(M/F) : M
	 * 성적 : 85
	 * 
	 * 3학년 4반 15번 홍길동 남학생의 성적은 85이다.
	 * */
	
	public void practice5() {
		String gen = "";
		Scanner sc = new Scanner(System.in);
		System.out.print("이름 : ");
		String name = sc.next();
		System.out.print("학년(숫자만) : ");
		int year = sc.nextInt();
		System.out.print("반(숫자만) : ");
		int cl = sc.nextInt();
		System.out.print("번호(숫자만) : ");
		int num = sc.nextInt();
		System.out.print("성별(M/F) : ");
		String gender = sc.next();
		System.out.print("성적 : ");
		String grade = sc.next();
		if(gender.equals("M")) {
			gen = "남";
		} else if(gender.equals("F")) {
			gen = "여";
		} else {
			System.out.println("M/F만 입력해주세요.");
			System.exit(0);
		}
		
		System.out.println(year+"학년 "+cl+"반 "+num+"번 "+name+" "+gen+"학생의 성적은 "+grade+"이다.");
		
	}

	/* 메소드명 : practice6() */
	/* 주민번호를 이용하여 남자인지 여자인지 구분하여 출력하세요 
	 * 
	 * -- 출력 예시--
	 * 주민번호를 입력하세요(-포함) : 123456-2123223
	 * 여자
	 * */

	public void practice6() {
		Scanner sc = new Scanner(System.in);
		System.out.print("주민번호를 입력하세요(-포함) : ");
		char idx = sc.nextLine().charAt(7);
		System.out.println(idx);
		if(idx == '1') {
			System.out.println("남자");
		}else if(idx == '2'){
			System.out.println("여자");
		}else {
			System.out.println("모르겠습니다.");
		}
	}

}
